
 ![sqlite](images/1_Czi9RSFob0UQ51Vx_1NzrA.png)
 # **Interaction d’une base de données avec Python** 

![sqlitenet](images/sqlitenet.png)

### ![](images/database_1_256.png)**Le module sqlite3**

Le module permettant d’intégrer un SGBD à un environnement Python s’appelle sqlite3 ; une fois ce dernier
importé, on se connecte à une base de données par l’intermédiaire de la fonction connect, en précisant en paramètre
un chemin d’accès vers la base de données.


```python
import sqlite3 as sql
# Changer le chemin ou utiliser os.chdir
import os
# os.chdir('bases')
connexion = sql.connect("communes_francaises.sqlite")
# id code(unique ville) | postal(code postale) |nom |superficie(km²) |population(milliers) |altitude(m) | dep_id : 59*** --> 60
```

L’objet ``connexion`` est désormais en place, et vous allez pouvoir dialoguer avec lui à l’aide du langage SQL. On va
d’abord mettre en place ce que l’on appelle un curseur. Il s’agit d’une sorte de tampon mémoire intermédiaire, destiné
à mémoriser temporairement les données en cours de traitement, ainsi que les opérations que vous effectuez sur elles,
avant leur transfert définitif dans la base de données. Cette technique permet donc d’annuler si nécessaire une ou
plusieurs opérations qui se seraient révélées inadéquates (dans le cas où on modifie la base de données), et de revenir
en arrière dans le traitement, sans que la base de données n’en soit affectée.


```python
curseur = connexion.cursor()
```

Une fois le curseur créé, la méthode ``execute`` du curseur permet de transmettre des requêtes rédigées en SQL sous
forme de chaîne de caractères.


```python
requete = curseur.execute("SELECT * FROM communes WHERE nom='CAMBRAI'")
```

Dans le cas où l’on effectue des requêtes d’extraction de données dans la base (SELECT ...), on peut parcourir le
curseur comme un itérable (et par exemple afficher les résultats)


```python
for resultat in curseur :
    print(resultat)
```

**![](images/database_1_256.png) Pour récupérer la première ligne (et 'déplacer' le curseur sur la ligne suivante), on peut écrire :**


```python
curseur.close()
curseur = connexion.cursor()
curseur.execute("SELECT * FROM communes WHERE dep_id=60 ORDER BY population DESC")
ligne = curseur.fetchone()
print(ligne)
```

**![](images/database_1_256.png) Vous pouvez aussi récupérer toutes les lignes renvoyés sous forme d'une liste (ce qui vide le curseur) :**


```python
curseur.close()
curseur = connexion.cursor()
curseur.execute("SELECT * FROM communes WHERE dep_id=60 AND population<0.2")
lignes = curseur.fetchall()
for ville in lignes :
    print(ville)
```

Ceci va vider le curseur, et l’on peut récupérer les résultats de la requête. Si l’on avait effectué plusieurs requêtes avant
de « vider » le curseur, les résultats se seraient « enfilés » dans le curseur.

Enfin, si des modifications ont été effectuées sur la BDD, il faut appliquer la méthode ``commit`` à la connexion créée pour qu’elles deviennent définitives. On peut ensuite refermer le curseur et la connexion.


```python
connexion.commit() # utile si une modification a eu lieu
curseur.close()
connexion.close()
```

### ![](images/database_1_256.png) **Script permettant un dialogue interactif avec la base de données**


```python
import sqlite3 as sql
connexion = sql.connect("communes_francaises.sqlite") #changer le répertoire
curseur = connexion.cursor()
while True:
    requete = input("Entrez une requête SQL ou STOP pour arrêter : ")
    if requete == "STOP":
        break
    try:
        curseur.execute(requete)
        for result in curseur:
            print(result)
    except:
         print("Requête incorrecte")
connexion.commit()
curseur.close()
connexion.close()
```

### ![](images/database_1_256.png) **Principales instructions utilisables pour manipuler une BDD**

|Instruction |Effet produit|
|:---:|:---:|
|`connexion = sqlite3.connect(fichierDonnees)` |Ouvre `connexion` qui permet d’accéder à la Bdd|
|`curseur =connexion.cursor()` |Crée un curseur cur qui agit sur la Bdd et permet de récupérer les résultats des requêtes|
|`connexion.commit()` |Exécute toutes les modifs sur la Bdd|
|`curseur.close()`| Ferme le curseur `curseur`|
|`connexion.close()` |Ferme la connexion `connexion`|
|`curseur.execute("CREATE TABLE membres (age INTEGER, nom TEXT, taille REAL)")` |Crée une table en indiquant les noms et types des champs|
|`curseur.execute("CREATE TABLE IF NOT EXISTS membres (age INTEGER, nom TEXT, taille REAL)") `|Idem mais teste si cette table existe|
|`curseur.execute("INSERT INTO membres(age,nom,taille) VALUES(21,'Dupont',1.83)") `|Insère dans la table et dans les champs cités les données suivantes|
|`curseur.execute("SELECT * FROM membres") `|Un exemple de SELECT|
|`curseur.execute("UPDATE membres set nom ='Gerart' WHERE nom='Ricard'") `|On remplace le nom 'Ricard' de la table membre par le nom 'Gerart'|
|`curseur.execute("DELETE FROM membres WHERE nom='Machin'")` |Supprime l’enregistrement vérifiant le critère|
|`curseur.execute("DROP TABLE nom_de_la_table") `|On supprime la table nom_de_la_table|
|`connexion = sqlite3.connect(':memory:')` | Création d'une base de données dans la RAM|
